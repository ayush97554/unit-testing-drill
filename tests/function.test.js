 const functions =require('../function')
//  beforeEach(()=>initDatabase());
//  afterEach(()=>closeDatabase());
beforeAll(()=>initDatabase());
 afterAll(()=>closeDatabase());

 const initDatabase =()=>console.log('Database Initialized...')
 const closeDatabase =()=>console.log('Database Initialized...')
 const nameCheck=()=>console.log('Checking Name...');

 describe('Checking Names',()=>{
     beforeEach(()=>nameCheck());
     test('User is Ayush',()=>{
         const user='Ayush';
         expect(user).toEqual('Ayush')
     })
 })
 test('Adds 2 + 2 to equal 4',()=>{
     expect(functions.add(2,2)).toBe(4);
 })
 test('Adds 2 + 2 to not equal 5',()=>{
    expect(functions.add(2,2)).not.toBe(5);
})
test('is Null',()=>{
    expect(functions.isNull()).toBeNull();
})
test('should be falsy',()=>{
   expect(functions.checkValue(undefined)).toBeFalsy();
})  
// toEqual
 test('User should be Ayush Tiwari object',()=>{
     expect(functions.createUser()).toEqual({
         firstName:"Ayush",
         lastName:"Tiwari"
     })
 })
//  Less than and greater than
// toBeLessThan
// toBeEqual
// toBeLessThanEqualTo
// toBeGreaterThanEqualTo
test('Should be under 1600',()=>{
    const load1=800;
    const load2=700;
    expect(load1+load2).toBeLessThan(1600);
})
// Regex
test('There is no I in team',()=>{
    expect('team').not.toMatch(/I/)
})
//  Arrays
test('Admin should be in usernames',()=>{
    usernames=['john','karen','admin'];
    expect(usernames).toContain('admin');
})

// working with async data

test('user fetched  name should be Leanne Graham',async()=>{
    const data=await functions.fetchUser()
        expect(data.name).toEqual('Leanne Graham')
    
})