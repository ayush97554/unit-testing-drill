const reverseString = require('../reversestring');


test('Is function reverse string exist',()=>{
    expect(reverseString).toBeDefined();
});

test('does reverse string function works',()=>{
    expect(reverseString('hello')).toEqual('olleh');
})